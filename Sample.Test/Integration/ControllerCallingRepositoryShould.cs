﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Sample.Data.Context;
using Sample.Data.Entities;
using Sample.WebApp.Controllers;
using Sample.WebApp.Models;
using Sample.WebApp.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Sample.Test.Integration
{
    public class ControllerCallingRepositoryShould
    {
        [Fact]
        public async void RedirectToDetailsAfterCreate()
        {
            var repo = SetupRepository();
            var sut = new CustomersController(repo);
            var result = await sut.Create(new CustomerVM());
            Assert.NotNull(result);
            var okRedirectResult = Assert.IsType<RedirectToActionResult>(result);
            var redirectResult = okRedirectResult as RedirectToActionResult;
            var action = redirectResult.ActionName;
            Assert.Equal("Details", action);

        }

        [Fact]
        public async void ReturnACustomerListFromIndex()
        {
            var repo = SetupRepository();
            var sut = new CustomersController(repo);
            var result = await sut.Index();
            Assert.NotNull(result);
            var okViewResult = Assert.IsType<ViewResult>(result);
            var viewResult = okViewResult as ViewResult;
            var modelResult = Assert.IsType<List<CustomerVM>>(viewResult.Model);
            var customerList = viewResult.Model as List<CustomerVM>;
            Assert.Equal(10, customerList.Count);
        }

        [Fact]
        public async void ReturnACustomerListFromOldest()
        {
            var repo = SetupRepository();
            var sut = new CustomersController(repo);
            var result = await sut.Oldest();
            Assert.NotNull(result);
            var okViewResult = Assert.IsType<ViewResult>(result);
            var viewResult = okViewResult as ViewResult;
            var modelResult = Assert.IsType<List<CustomerVM>>(viewResult.Model);
            var customerList = viewResult.Model as List<CustomerVM>;
            // Oldest list should be restricted to 5
            Assert.Equal(5, customerList.Count);
        }

        private ICustomerRepository SetupRepository()
        {
            var optionsBuilder = new DbContextOptionsBuilder<ApplicationDBContext>();
            optionsBuilder.UseInMemoryDatabase("integrationtest");
            var context = new ApplicationDBContext(optionsBuilder.Options);
            InitializeDbForTests(context);
            var repo = new CustomerRepository(context);
            return repo;
        }

        private void InitializeDbForTests(ApplicationDBContext db)
        {
            for (int i = 1; i <= 10; i++)
            {
                var customer = new Customer("Tim" + i, "Smith" + i, "t@x.com", new DateTime(day: 28 - i, month: 2, year: 1960));
                db.Add(customer);
            }
            db.SaveChanges();
        }
    }
}
