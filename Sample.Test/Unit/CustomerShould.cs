﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using Sample.Data.Entities;


namespace Sample.Test.Unit
{
    public class CustomerShould
    {
        [Fact]
        public void BeLowerCaseCustomerCode()
        {
            var customer = new Customer(firstName: "Test", lastName: "First", email: "ftest@x.com", dateOfBirth: new DateTime(day: 1, month: 5, year: 1990));
            Assert.DoesNotContain(" ", customer.CustomerCode);
            Assert.Equal(customer.CustomerCode, customer.CustomerCode.ToLower());
        }

        [Fact]
        public void HaveNoUpperCaseInCustomerCode()
        {
            var customer = new Customer(firstName: "Test", lastName: "First", email: "ftest@x.com", dateOfBirth: new DateTime(day: 1, month: 5, year: 1990));
            Assert.DoesNotContain(" ", customer.CustomerCode);
        }

        [Fact]
        public void CreateValidCustomerCodeWithValidData()
        {
            string expected = "testfirst19900501";
            var customer = new Customer(firstName: "Test", lastName: "First", email: "ftest@x.com", dateOfBirth: new DateTime(day:1, month:5, year:1990));
            Assert.Equal( expected, customer.CustomerCode );

        }
        [Fact]
        public void CreateValidCustomerCodeWithNullDOB()
        {
            string expected = "testfirst00000000";
            var customer = new Customer(firstName: "Test", lastName: "First", email: "ftest@x.com", dateOfBirth: null);
            Assert.Equal(expected, customer.CustomerCode);

        }

        [Fact]
        public void HaveValidFullNameAfterCreation()
        {
            string expected = "Test First";
            var customer = new Customer(firstName: "Test", lastName: "First", email: "ftest@x.com", dateOfBirth: new DateTime(day: 1, month: 5, year: 1990));
            Assert.Equal(expected, customer.FullName);
        }
    }
}
