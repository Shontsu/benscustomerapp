﻿using Sample.Data.Entities;
using Sample.WebApp.Models;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Sample.Test.Unit
{
    public class CustomerVMShould
    {
        [Fact]
        public void HaveAllFieldsPopulatedWhenCreatedWithCustomer()
        {
            var customer = new Customer("Tim", "Smith", "t@x.com", new DateTime(day: 1, month: 2, year: 1960));
            var customervm = new CustomerVM(customer);

            Assert.Equal("Tim", customer.FirstName);
            Assert.Equal("Smith", customer.LastName);
            Assert.Equal("Tim Smith", customer.FullName);
            Assert.Equal("t@x.com", customer.Email);
            Assert.Equal(1960, customer.DateOfBirth.HasValue?customer.DateOfBirth.Value.Year: 1950);
            Assert.Equal("timsmith19600201", customer.CustomerCode);
            Assert.Equal(customer.Id, customervm.Id);

        }
    }
}
