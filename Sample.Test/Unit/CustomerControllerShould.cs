﻿using Microsoft.AspNetCore.Mvc;
using Moq;
using Sample.Data.Entities;
using Sample.WebApp.Controllers;
using Sample.WebApp.Models;
using Sample.WebApp.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Sample.Test.Unit
{
    public class CustomerControllerShould
    {
        private Mock<ICustomerRepository> _mockRepository;

        public CustomerControllerShould()
        {
            _mockRepository = new Mock<ICustomerRepository>();
            _mockRepository.Setup(x => x.GetCustomer(It.IsAny<int>()))
                .Returns(Task.FromResult(getCustomer()));
            _mockRepository.Setup(x => x.GetCustomers())
                .Returns(Task.FromResult(getCustomerList()));
            _mockRepository.Setup(x => x.SaveCustomer(It.IsAny<CustomerVM>()))
                .Returns(Task.FromResult(getCustomer()));
        }

        [Fact]
        public async void ReturnACustomerFromDetails()
        {
            var sut = new CustomersController(_mockRepository.Object);
            var result = await sut.Details(1);
            Assert.NotNull(result);
            var okViewResult = Assert.IsType<ViewResult>(result);
            var viewResult = okViewResult as ViewResult;
            var modelResult = Assert.IsType<CustomerVM>(viewResult.Model);
            var customer = viewResult.Model as CustomerVM;
            Assert.Equal("FirstName", customer.FirstName);
        }

        [Fact]
        public async void RedirectToDetailsAfterCreate()
        {
            var sut = new CustomersController(_mockRepository.Object);
            var result = await sut.Create(new CustomerVM());
            Assert.NotNull(result);
            var okRedirectResult = Assert.IsType<RedirectToActionResult>(result);
            var redirectResult = okRedirectResult as RedirectToActionResult;
            var action = redirectResult.ActionName;
            Assert.Equal("Details", action);
            
        }

        [Fact]
        public async void ReturnACustomerListFromIndex()
        {
            var sut = new CustomersController(_mockRepository.Object);
            var result = await sut.Index();
            Assert.NotNull(result);
            var okViewResult = Assert.IsType<ViewResult>(result);
            var viewResult = okViewResult as ViewResult;
            var modelResult = Assert.IsType<List<CustomerVM>>(viewResult.Model);
            var customerList = viewResult.Model as List<CustomerVM>;

            // since it's using mocked repository, it will actually return 7 in this test.
            Assert.Equal(7, customerList.Count);
        }


        private IEnumerable<CustomerVM> getCustomerList()
        {
            List<CustomerVM> customerList = new List<CustomerVM>();
            for (int i = 1; i<= 7; i++)
            {
                var customer = new Customer("FirstName" + i, "LastName", "fl@x.com", new DateTime(day: 01 + i, month: 03, year: 1974));
                customerList.Add(new CustomerVM(customer));
            }
            return customerList;
        }

        private CustomerVM getCustomer()
        {
            var customer = new Customer("FirstName", "LastName", "fl@x.com", new DateTime(day: 21, month: 03, year: 1974));
            return new CustomerVM()
            {
                FirstName ="FirstName",
                LastName = "LastName",
                Email = "fl@x.com",
                DateOfBirth = new DateTime(day: 21, month: 03, year: 1974),
            };
        }
    }
}
