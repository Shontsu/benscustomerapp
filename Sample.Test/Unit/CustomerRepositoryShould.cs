﻿using Microsoft.EntityFrameworkCore;
using Sample.Data.Context;
using Sample.Data.Entities;
using Sample.WebApp.Models;
using Sample.WebApp.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace Sample.Test.Unit
{
    public class CustomerRepositoryShould
    {

        [Fact]
        public async void AddACustomer()
        {
            var context = SetupContext();
            var customerToSave = new CustomerVM(new Customer("Tim" , "Smith", "t@x.com", new DateTime(day: 1 , month: 2, year: 1960)));

            var sut = new CustomerRepository(context);
            var result = await sut.SaveCustomer(customerToSave);
            Assert.NotNull(result);

            var savedCustomer = await context.Customers.Where(c => c.Id == result.Id).FirstOrDefaultAsync();
            Assert.Equal(result.CustomerCode, savedCustomer.CustomerCode);
        }

        [Fact]
        public async void GetACustomerById()
        {
            var context = SetupContext();
            var customerToSave = new Customer("Tim" , "Smith", "t@x.com", new DateTime(day: 1 , month: 2, year: 1960));
            context.Add(customerToSave);
            await context.SaveChangesAsync();

            var sut = new CustomerRepository(context);
            var result = await sut.GetCustomer(customerToSave.Id);
            Assert.NotNull(result);
            Assert.Equal(customerToSave.CustomerCode, result.CustomerCode);
        }


        [Fact]
        public async void FetchARestrictedListOfCustomers()
        {
            var context = SetupContext();
            AddMultipleCustomersToContext(context, 7);

            var sut = new CustomerRepository(context);
            var result = await sut.GetOldestCustomers(5);
            Assert.NotNull(result);
            var resultList = result as List<CustomerVM>;
            Assert.Equal(5, resultList.Count);
            var orderedList = resultList.OrderBy(c => c.LastName);
            Assert.True(orderedList.SequenceEqual(resultList));
        }



        private ApplicationDBContext SetupContext()
        {
            var optionsBuilder = new DbContextOptionsBuilder<ApplicationDBContext>();
            optionsBuilder.UseInMemoryDatabase("unittest");
            var context = new ApplicationDBContext(optionsBuilder.Options);
            return context;
        }

        private async void AddMultipleCustomersToContext(ApplicationDBContext context, int numberToAdd)
        {
            for (int i = 1; i <= numberToAdd; i++)
            {
                var customer = new Customer("Tim" + i, "Smith" + i, "t@x.com", new DateTime(day: 28 - i, month: 2, year: 1960));
                context.Add(customer);
            }
            await context.SaveChangesAsync();
        }
    }
}
