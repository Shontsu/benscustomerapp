﻿using Sample.Data.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Sample.WebApp.Models
{
    public class CustomerVM
    {
        public CustomerVM() { }

        public CustomerVM(Customer c)
        {
            Id = c.Id;
            FirstName = c.FirstName;
            LastName = c.LastName;
            Email = c.Email;
            DateOfBirth = c.DateOfBirth;
            CustomerCode = c.CustomerCode;
            FullName = c.FullName;
        }

        public int Id { get; set; }

        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        [DataType(DataType.Date)]
        public DateTime? DateOfBirth { get; set; }
        [Editable(false)]
        public string CustomerCode { get; private set; }
        [Editable(false)]
        public string FullName { get; private set; }



    }
}
