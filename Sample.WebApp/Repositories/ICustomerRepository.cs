﻿using Sample.WebApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sample.WebApp.Repositories
{
    public interface ICustomerRepository
    {
        Task<CustomerVM> GetCustomer(int id);
        Task<IEnumerable<CustomerVM>> GetOldestCustomers(int size);
        Task<IEnumerable<CustomerVM>> GetCustomers();
        Task<CustomerVM> SaveCustomer(CustomerVM customervm);

    }
}
