﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Sample.Data.Context;
using Sample.Data.Entities;
using Sample.WebApp.Models;

namespace Sample.WebApp.Repositories
{
    public class CustomerRepository : ICustomerRepository
    {
        private ApplicationDBContext _context;

        public CustomerRepository(ApplicationDBContext context)
        {
            _context = context;
        }

        public async Task<CustomerVM> GetCustomer(int id)
        {
            var customer = await _context.Customers
                .Where(c => c.Id == id)
                .SingleOrDefaultAsync();

            if (customer != null)
            {
                return new CustomerVM(customer);
            } else
            {
                return null;
            }
        }

        public async Task<IEnumerable<CustomerVM>> GetCustomers()
        {
            return await _context.Customers
                .OrderBy(c => c.LastName)
                .Select(c => new CustomerVM(c))
                .ToListAsync();

        }

        public async Task<IEnumerable<CustomerVM>> GetOldestCustomers(int size)
        {
            return await _context.Customers
                .Where(c => c.DateOfBirth.HasValue)
                .OrderBy(c => c.DateOfBirth)
                .Take(size)
                .OrderBy(c => c.LastName)
                .Select(c => new CustomerVM(c))
                .ToListAsync();

        }

        public async Task<CustomerVM> SaveCustomer(CustomerVM customervm)
        {
            Customer customer = new Customer(customervm.FirstName, customervm.LastName, customervm.Email, customervm.DateOfBirth);
            _context.Add(customer);
            await _context.SaveChangesAsync();
            return new CustomerVM(customer);
        }
    }
}
