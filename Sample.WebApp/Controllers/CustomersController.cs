﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Sample.Data.Context;
using Sample.Data.Entities;
using Sample.WebApp.Models;
using Sample.WebApp.Repositories;

namespace Sample.WebApp.Controllers
{
    public class CustomersController : Controller
    {
        private readonly ICustomerRepository _repository;

        public CustomersController(ICustomerRepository repository)
        {
            _repository = repository;
        }


        // GET: Oldest Customers
        public async Task<IActionResult> Oldest()
        {
            return View(await _repository.GetOldestCustomers(5));
        }

        // GET: Customers
        public async Task<IActionResult> Index()
        {
            return View(await _repository.GetCustomers());
        }

        // GET: Customers/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var customer = await _repository.GetCustomer(id??0);
            if (customer == null)
            {
                return NotFound();
            }

            return View(customer);
        }

        // GET: Customers/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Customers/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,FirstName,LastName,Email,DateOfBirth")] CustomerVM customervm)
        {
            if (ModelState.IsValid)
            {

                customervm = await _repository.SaveCustomer(customervm);
                return RedirectToAction(nameof(Details),new { id = customervm.Id });
            }
            return View(customervm);
        }

        
    }
}
