﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Sample.Data.Entities
{
    public class Customer
    {
        private string _customerCode;

        private Customer() { }

        public Customer(string firstName, string lastName, string email, DateTime? dateOfBirth)
        {
            FirstName = firstName;
            LastName = lastName;
            Email = email;
            DateOfBirth = dateOfBirth;
            _customerCode = CreateCustomerCode(FullName, dateOfBirth);
        }

        
        [Key]
        public int Id { get; set; }

        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        [DataType(DataType.Date)]
        public DateTime? DateOfBirth { get; set; }
        public string CustomerCode {
            get { return _customerCode; }
        }
        public string FullName
        {
            get { return FirstName + " " + LastName; }
        }

        private string CreateCustomerCode(string fullName, DateTime? dateOfBirth)
        {
            return fullName.ToLower().Replace(" ", string.Empty) + (dateOfBirth.HasValue? dateOfBirth.Value.ToString("yyyyMMdd"):"00000000");
        }
    }
}
