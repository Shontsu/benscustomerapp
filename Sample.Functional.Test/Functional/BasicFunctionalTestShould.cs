﻿using Microsoft.AspNetCore.Mvc.Testing;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Sample.Test.Functional
{
    public class BasicFunctionalTestShould : IClassFixture<CustomWebApplicationFactory<Sample.WebApp.Startup>>
    {
        private readonly CustomWebApplicationFactory<Sample.WebApp.Startup> _factory;

        public BasicFunctionalTestShould(CustomWebApplicationFactory<Sample.WebApp.Startup> factory)
        {
            _factory = factory;
        }

        [Theory]
        [InlineData("/")]
        [InlineData("/Customers/Index")]
        [InlineData("/Customers/Create")]
        [InlineData("/Customers/Details/1")]
        public async Task Get_EndpointsReturnSuccessAndCorrectContentType(string url)
        {
            // Arrange
            var client = _factory.CreateClient();

            // Act
            var response = await client.GetAsync(url);

            // Assert
            response.EnsureSuccessStatusCode(); // Status Code 200-299
            Assert.Equal("text/html; charset=utf-8",
                response.Content.Headers.ContentType.ToString());
        }

        [Fact]
        public async Task GetCustomerFromValidId()
        {
            // Arrange
            var client = _factory.CreateClient();

            // Act
            var response = await client.GetAsync("/Customers/Details/1");

            // Assert
            response.EnsureSuccessStatusCode(); // Status Code 200-299
            var stringResponse = await response.Content.ReadAsStringAsync();
            Assert.Contains("Tim1", stringResponse);
        }
    }
}
